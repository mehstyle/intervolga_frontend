import axios from "axios";

const BASE_URL = "http://localhost:5000";

const configureApi = () => {
    const instance = axios.create({
        baseURL: BASE_URL
    });

    instance.interceptors.request.use((config) => {
        return config;
    }, (error) => {
        console.log(error);

        return Promise.reject(error);
    });

    instance.interceptors.response.use((response) => {

        return response;
    }, (error) => {
        if (error && error.response && error.response.status) {
            if (error.response.status === 401) {
            }
        }

        return Promise.reject(error);
    });

    return instance;
};

const api = configureApi();

export default {
    files(url) {
        return api.get("get_repo_tree/?url=" + url);
    }
}
