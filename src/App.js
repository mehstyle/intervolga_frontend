import React, { Component } from "react";
import fileApi from "../src/api/index.js";
import Graph from "react-graph-vis";
import "./App.css";

// https://github.com/Aleksandr-Kuz/TestProject

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            files: [],
            nodes: [
                {
                    id: "0",
                    label: "test"
                }
            ],
            edges: [
                {
                    from: 0,
                    to: 1
                }
            ],
            currentLevel:0,
            repoUrl:undefined,
            isLoading:false,
            nodeContent:undefined
        };

        this.updateRepoUrl = this.updateRepoUrl.bind(this);
        this.createNode = this.createNode.bind(this);
    }

    validURL(str) {
        const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
    }

    updateRepoUrl(event) {
        this.setState({
            repoUrl: event.target.value
        })
    }

    createNode() {
        this.setState({isLoading:true});
        fileApi.files(this.state.repoUrl).then(res => {
            const files = res.data;
            this.setState({ files });
            let nodes = [];
            let edges = [];

            let classes = [];

            for (let i = 0; i < files.length; i++) {
                let classesNames = "";
                let usedClassesNames = "";

                for (let j = 0; j < files[i].classes.length; j++) {
                    classesNames += "\n" + files[i].classes[j].name + " line:" + files[i].classes[j].string_numb;
                    classes.push({name:files[i].classes[j].name, fileIndex:i})
                }

                for (let j = 0; j < files[i].used_classes.length; j++) {
                    usedClassesNames += "\n" + files[i].used_classes[j].name  + " line:" + files[i].used_classes[j].string_numb;
                }

                nodes.push({ id: i, label: files[i].name + "\n\nclasses:" + classesNames + "\n\nused classes:" + usedClassesNames, title:"test"});
            }

            for (let i = 0; i < files.length; i++) {
                files[i].used_files = [];
                if (files[i].used_classes.length !== 0) {
                    for (let j = 0; j < files[i].used_classes.length; j++) {
                        files[i].used_files.push(classes.find((element) => {return element.name === files[i].used_classes[j].name}).fileIndex);
                    }
                }
            }

            for (let i = 0; i < files.length; i++) {
                for (let j = 0; j < files[i].used_files.length; j++) {
                        edges.push({ from: i, to: files[i].used_files[j], dashes:[5,0] });
                }

                for (let j = 0; j < files[i].classes.length; j++) {

                    if (files[i].classes[j].parent.length > 0) {
                        let indexTo =  classes.find((element) => {return element.name === files[i].classes[j].parent[0]}).fileIndex;
                        edges.push({ from: i, to: indexTo, dashes:[5,5] });
                    }


                }
            }

            this.setState({ nodes, edges });
            this.setState({isLoading:false});
        });
    }

    render() {

        if (this.state.nodes.length < 1) return <div />;

        const graph = {};
        graph.nodes = this.state.nodes;
        graph.edges = this.state.edges;

        var options = {
            layout: {
                hierarchical: {
                    direction: "LR",
                    sortMethod: "directed",
                    levelSeparation: 300,
                    nodeSpacing: 300
                }
            },
            physics: {
                enabled: false
            },
            height: 1000
        };

        var events = {
            select: function(event) {
                var { nodes, edges } = event;
                console.log("test");
            }
        };

        return (
            <div>
            <div>
                <div className="input-group input-group-lg">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-lg">Repo url</span>
                    </div>
                    <input type="text" className="form-control" aria-label="Large"
                           aria-describedby="inputGroup-sizing-sm" value={this.state.repoUrl} onChange={this.updateRepoUrl}/>
                    {this.validURL(this.state.repoUrl) ? <button type="button" className="btn btn-primary" onClick={this.createNode}>Create tree</button> : <div></div>}
                </div>
            </div>
                {!this.state.isLoading ? <div></div> :
                    <div>
                        Loading...
                    </div>
                }
                {!this.validURL(this.state.repoUrl) || this.state.files.length < 1 ? <div></div> :
                    <div>
                        <Graph graph={graph} options={options} onKeyPress={events} />
                    </div>
                }
                <hr />
                <div>
                    {this.state.nodeContent === undefined ? this.state.nodeContent : ""}
                </div>
            </div>
            );
    }
}

export default App;